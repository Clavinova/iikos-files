#include <stdio.h>  /* printf */
#include <unistd.h> /* fork */

int main(void) {
  int p;

  p=fork();
  printf("\n fork returnerer pid = %d\n", p);

  return 0;
}
